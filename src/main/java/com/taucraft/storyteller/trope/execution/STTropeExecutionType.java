package com.taucraft.storyteller.trope.execution;

public enum STTropeExecutionType {
    FARAWAY(0),
    NEARBY(1),
    FULL(2);

    private final int id;

    STTropeExecutionType(int id) {
        this.id = id;
    }
    public static STTropeExecutionType fromNum(int num) {
        return STTropeExecutionType.values()[num];
    }

    public int toNum() {
        return id;
    }

}
