package com.taucraft.storyteller.trope.execution;

import com.taucraft.storyteller.trope.STTrope;
import org.jetbrains.annotations.Nullable;

public abstract class STTropeExecutionCheck<T extends STTrope> {

    public abstract STTropeExecutionType handle(T trope);
    public abstract Class<T> getDataGettingClass();

    @Nullable
    public STTropeExecutionType handleInternal(STTrope trope) {
        if (getDataGettingClass().isInstance(trope)) {
            return handle(getDataGettingClass().cast(trope));
        }
        return null;
    }
}
