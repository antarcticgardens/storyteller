package com.taucraft.storyteller.trope;

import com.taucraft.storyteller.STUniverse;
import org.jetbrains.annotations.NotNull;

public class STTrope {
    private final STUniverse universe;
    private boolean completed;

    public STTrope(@NotNull STUniverse universe) {
        this.universe = universe;
        completed = false;
    }

    public STUniverse getUniverse() {
        return universe;
    }

    public boolean isComplete() {
        return completed;
    }

    public void finish() {
        this.completed = true;
    }

    public void tick(long deltaTime) {

    }

}
