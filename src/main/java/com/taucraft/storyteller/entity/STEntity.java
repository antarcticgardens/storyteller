package com.taucraft.storyteller.entity;

import com.taucraft.storyteller.STUniverse;
import com.taucraft.storyteller.entity.component.STComponent;
import com.taucraft.storyteller.entity.parents.STMovementCause;
import com.taucraft.storyteller.entity.parents.STParentMovementResult;
import com.taucraft.storyteller.entity.parents.STParentingType;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents an entity in the universe. The term entity can be used broadly, so a magic system could also be considered
 * an entity. In general, anything that someone can have an opinion on should be an entity.
 */
public class STEntity {

    private STUniverse universe;
    private List<STEntity> children;
    /**
     * Parents can represent anything theoretically as long as it
     * makes sense for that entity to be a parents, examples would be:
     * - Their fantasy race/species
     * - The village they are in
     * - Their political group
     */
    private List<STEntity> parents;
    private List<STComponent> components;

    public STEntity(@NotNull STUniverse universe) {
        this.universe = universe;
        this.children = new ArrayList<>();
        this.parents = new ArrayList<>();
        this.components = new ArrayList<>();
    }

    @Nullable
    public <T extends STComponent> T getComponent(@NotNull Class<T> type) {
        for (STComponent component : components) {
            if (type.isInstance(component)) {
                return type.cast(component);
            }
        }
        return null;
    }

    public <T extends STComponent> List<T> getComponents(@NotNull Class<T> type) {
        List<T> comps = new ArrayList<>();
        for (STComponent component : components) {
            if (type.isInstance(component)) {
                comps.add(type.cast(component));
            }
        }
        return comps;
    }

    @Nullable
    public STParentingType getParentingType() {
        return null;
    }

    /**
     * Parents another entity to this entity
     * @param parent the parent
     * @return Success of parenting
     */
    public boolean parent(@NotNull STEntity parent) {
        return parent(parent, null);
    }

    /**
     * Ignores ParentingType of parent.
     * May be useful when adding a lot of parents that have a parenting type for speed.
     * In general, if not proven that {@link #parent(STEntity) parent} is a bottleneck, use it.
     * @param parent parent
     */
    public void fastParent(@NotNull STEntity parent) {
        parent.children.add(this);
        parents.add(parent);
    }

    /**
     * Parents another entity to this entity
     * @param parent the parent
     * @param movementCause The cause of this parenting, should be a class extending movementCause that explains it
     * @return Success of parenting
     */
    public boolean parent(@NotNull STEntity parent, @Nullable STMovementCause movementCause) {
        STParentingType type = parent.getParentingType();
        if (type != null) {
            List<STEntity> toRemove = new ArrayList<>();
            for (STEntity p : parents) {
                if (p.getParentingType() == type) {
                    STParentMovementResult result = type.isMovementPossible(p, parent, movementCause);
                    switch (result) {
                        case SUCCESS -> toRemove.add(p);
                        case MULTI_PARENT_IS_POSSIBLE -> {}
                        case FAILURE -> {return false;}
                    }
                }
            }
            for (STEntity stEntity : toRemove) {
                parents.remove(stEntity);
                parent.children.remove(this);
            }
            parent.children.add(this);
            parents.add(parent);
        }
        return true;
    }


}
