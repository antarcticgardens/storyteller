package com.taucraft.storyteller.entity.parents;

import com.taucraft.storyteller.entity.STEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class STParentingType {

    @NotNull
    public STParentMovementResult isMovementPossible(@NotNull STEntity from, @NotNull STEntity to, @Nullable STMovementCause cause) {
        return STParentMovementResult.MULTI_PARENT_IS_POSSIBLE;
    }

}
