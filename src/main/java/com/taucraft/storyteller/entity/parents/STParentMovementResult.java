package com.taucraft.storyteller.entity.parents;

public enum STParentMovementResult {
    SUCCESS,
    FAILURE,
    MULTI_PARENT_IS_POSSIBLE
}
