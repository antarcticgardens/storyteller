package com.taucraft.storyteller;

import com.taucraft.storyteller.entity.STEntity;
import com.taucraft.storyteller.trope.STTrope;
import com.taucraft.storyteller.trope.execution.STTropeExecutionCheck;
import com.taucraft.storyteller.trope.execution.STTropeExecutionType;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Universe in which entities exist and events happen.
 */
public class STUniverse {
    private final List<STEntity> entities;

    private final List<STTropeExecutionCheck<?>> tropeExecutionChecks;

    public STUniverse(List<STTropeExecutionCheck<?>> tropeExecutionChecks) {
        entities = new ArrayList<>();
        this.tropeExecutionChecks = tropeExecutionChecks;
    }

    public void addEntity(STEntity entity) {
        entities.add(entity);
    }

    public STTropeExecutionType getTropeExecutionType(STTrope trope) {
        STTropeExecutionType type = STTropeExecutionType.FARAWAY;

        for (STTropeExecutionCheck<? extends STTrope> tropeExecutionCheck : tropeExecutionChecks) {
            var result = tropeExecutionCheck.handleInternal(trope);
            if (result != null && result.toNum() > type.toNum()) {
                type = result;
            }
        }

        return type;
    }




}
